function handleEditProfile() {
    var name = document.getElementById("name").textContent
    var inputname = document.getElementById("input-name")
    inputname.value = name

    var email = document.getElementById("email").textContent
    var inputemail = document.getElementById("input-email")
    inputemail.value = email

    var interest = document.getElementById("interests").textContent
    var inputinterest = document.getElementById("input-interest")
    inputinterest.value = interest

    document.getElementById("edit-view").style.display = "block"
    document.getElementById("display-view").style.display = "none"

}
function handleupdateProfile() {
  
    var updateName = document.getElementById("input-name").value
    var name = document.getElementById("name")
    name.textContent = updateName
  
    var updateemail = document.getElementById("input-email").value
    var email = document.getElementById("email")
    email.textContent = updateemail
   
    var updateinterest = document.getElementById("input-interest").value
    var interest = document.getElementById("interests")
    interest.textContent = updateinterest
    
    document.getElementById("edit-view").style.display = "none"
    document.getElementById("display-view").style.display = "block"

}